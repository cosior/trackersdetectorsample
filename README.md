```
Trackers Detective - Detect third party domains while you surf the web.
Copyright (C) 2015  Costas Iordanou - costasjordanou@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
```

__Folder Structure__
***
```
Trackers/                              # The root folder
    |
    |-- _locales/                      # The localization folder
    |       |
    |       |--en/                     # The localization folder for the english language messages
    |           |
    |           |-- messages.json      # The english language messages file
    |
    |-- icons/                         # The plugin icons folder
    |     |
    |     |-- detective.png            # The plugin icon file
    |
    |-- src/                           # The source code folder
    |    |
    |    |--bg/                        # The background process code folder
    |    |   |
    |    |   |-- background.js         # The main background javascript code
    |    |   |-- bg_support.js         # The support javascript code file for the backgroung process
    |    |   |-- localStorage.js       # The localStorage javascript support code file
    |    |
    |    |--browser_action/            # The browser action code folder (popup handler)
    |    |        |
    |    |        |-- popup.css        # The css style file for popup.html
    |    |        |-- popup.html       # The html code of the popup window
    |    |        |-- popup.js         # The javascript code for the popup window
    |    |
    |    |--page_action/               # The injected javascript code folder
    |            |
    |            |-- inject_script.js  # The injected javascript code for each tab
    |
    |--manifest.json                   # The main manifest file of the plugin
    |--README.txt                      # This file
```
***

**Current functionalities:**

1. User options sample for two option variables (option_1, option_2).
2. Unique user ID example. When user tracking is required.
3. Third party requests monitoring. Monitor all third party requests and store them in
   a dataset for each open tab excluding internal browser pages.
4. Cookies monitoring. Monitor all outgoing http headers and extract the 'Cookie' field
   values. For each third party domain we aggregate the cookie values into a key value
   object i.e. { 'thirdPartyDomain': [cookie1, cookie2, ...] }. All third party domain
   cookies are stored within the corresponding tab dataset under dataset.cookies --> i.e.
   [{ 'thirdPartyDomain1': [cookie1, cookie2, ...] }, { 'thirdPartyDomain2': [...]}, ...]
5. Implementation of message parsing between all component code parts (background,
   popup and inject_script).
6. LocalStorage wrapper for storing tabs' dataset in a persistent storage (Currently not
   in use).
7. Badge update functions (annotate the plugin logo image with the number of unique third
   party requests) to inform the user in real time.
8. Present a list of third part domains to user when the logo image is clicked.
9. Options to collect all possible arguments from an outgoing HTTP GET request.
   i.e. http://tracker.com?userID=1234&counry=US&...

All API object documentation is avaialbe here 'https://developer.chrome.com/extensions/api_index'
For more details see inline comments in each file.

To install the plugin for testing go to Setting->Extensions and select the 'Developer mode'
checkbox at the right top of the page. Then click on 'Load unpacked extension...' button and
select the root folder of the plugin, 'Trackers' in our case.