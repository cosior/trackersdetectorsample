/* Trackers Detective - Detect third party domains while you surf the web.
 * Copyright (C) 2015  Costas Iordanou - costasjordanou@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

/*
 *You can use this file to seperate some functions from the background.js script.
 *I use it to have more clean background.js file. It's up to you if you want to
 *use it. If not, you have to copy all this functions into background.js
 */

// Function to create unique user id (uuid).
// Use this if you want to track your users
var create_uuid = function(){
  //Create new user unique ID
  var time = Date.now();
  var randomSeed = Math.floor( Math.random() * 10000000000 ) + 1;
  var tmp = time * randomSeed;
  // Create a random number (up) and cast it to 12 character string (below).
  var uuid = tmp.toString( 36 ).substring( 0, 12 );
  // Make it available globaly
  USER_UID = uuid;
  return uuid;
};

//Function to extract the top level domain from a full url
var extract_domain = function(url){
  //Remove the 'http(s)://' part 
  var tmp = url.split('//')[1]
  //Remove the rest of the url except the domain
  tmp = tmp.split('/')[0]
  //Get only the actual part of the domain
  var parts = tmp.split('.').reverse();
  var cnt = parts.length;
  if (cnt >= 3) {
    if (parts[1].match(/^(com|edu|gov|net|mil|org|nom|co|name|info|biz)$/i)) {
      return parts[2] + '.' + parts[1] + '.' + parts[0];
    }
  }
  domain = parts[1] + '.' + parts[0];
  return domain;
};

//Function to update the plugin icon with the number of detected third partiy domains
var setBadgeText = function(color, id){
  if (tabsArray[id] != undefined) {
    var thirdParties = tabsArray[id].thirdParties;
    var thirdParties_num =  Object.keys(thirdParties).length;
    chrome.browserAction.setBadgeBackgroundColor({color:color, tabId: id});
    if (thirdParties_num == 0) {
      chrome.browserAction.setBadgeText({text: '', tabId: id});
    } else {
      chrome.browserAction.setBadgeText({text: (thirdParties_num).toString(), tabId: id});
    }
  } else {
    chrome.browserAction.setBadgeText({text: '', tabId: id });
  }
  //Skip the error when the tabs is already closed.
  if (chrome.runtime.lastError) { console.log(chrome.runtime.lastError.message); }
};

//Support function to insert detected cookies into the tab datset
var recordCookies = function( cookies, id ) {
  if (debug) console.log('recordCookies');
  if (debug) console.log(cookies);
  if (debug) console.log(id);
  var dataset = tabsArray[ id ]; //Get tab's dataset
  if (dataset == undefined) return; //This is a new tab, ignore it.
  if (debug) console.log(dataset);
  var cookieList = dataset.cookies; //Get the cookies List
  var domain = cookies.domain;
  if (debug) console.log(domain);
  var name = cookies.name;
  var value = cookies.value;
  var finalCookie = name + '=' + value;
  //Support variables to update existing cookies
  var old, tmp = undefined;
  var found = false;
  //Loop through all existing domains to update their cookies
  for ( var j = 0; j < cookieList.length; j++ ) {
    tmp = cookieList[j];
    if (debug) console.log(tmp);
    if ( tmp.hasOwnProperty( domain ) ) {
      old = tmp[ domain ].split( ';' ); //Split the string of cookies into singles
      old = old.filter( function( v ){ return v !== "" } ); //Clean empty elements
      if ( old.indexOf( finalCookie ) > -1) { } //Cookie already Exists 
      else { old.push( finalCookie ); } //Add the new one
      tmp[ domain ] = old.join( ';' ); //Convert them back to string
      found = true; //Skip new domain insertion below
      dataset.cookies[j] = tmp; // Update the dataset
      break;
    }
  }
  if ( !found ) { //Domain not exists. Insert it alongside the cookie
    obj = {};
    obj[ domain ] = finalCookie;
    dataset.cookies.push( obj );
  }
  // Finally store the dataset back to the tabsArray.
  tabsArray[ id ] = dataset;
  if (debug) console.log('Updated 3rd Party info.');
  if (debug) console.log(tabsArray);
};

// show notification message to user.
var showUserMessage = function( title, message ) {
  chrome.notifications.create( {
    type: "basic",
    title: title,
    message: message,
    iconUrl: "../../icons/detective.png"
  });
};