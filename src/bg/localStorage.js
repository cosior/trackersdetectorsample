/* Trackers Detective - Detect third party domains while you surf the web.
 * Copyright (C) 2015  Costas Iordanou - costasjordanou@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

/*Use the following factions to store data using the local storage
 *This is optional in case you need it. I alredy include all necesary
 *permissions in the manifest file for you.
 */

// Get storage element size. Use 'null' to get the overall size.
var getStorageElementSize = function( element, callback ) {
  chrome.storage.local.getBytesInUse( element, callback );
};

// Set storage element.
var setStorageElement = function( element, callback ) {
  chrome.storage.local.set( element, callback );
};

// Get storage elements values. Use 'null' to get all available elements. 
var getStorageElement = function( element, callback ) {
  chrome.storage.local.get( element, callback );
};

// Remove single element from local storage.
var removeStorageElement = function( element ) {
  chrome.storage.local.remove( element, function() {
    if (chrome.runtime.lastError != undefined) { // Set only at error.
      console.log('Error on removeStorageElement.');
      console.log(chrome.runtime.lastError);
    }
  });
};

// Clean all elements from local storage except unique user id (uuid) if exists
var cleanStorage = function() {
  //First get the user_uuid...
  getStorageElement(OPTION_OBJ, function(res){
    //...then clean localStorage,...
    chrome.storage.local.clear( function() {
      //...and finally save back the user_uuid.
      setStorageElement(createStorageElement(OPTION_OBJ, res[OPTION_OBJ]));
      if (chrome.runtime.lastError != undefined) { // Set only at error.
        console.log('Error on cleanStorage.');
        console.log(chrome.runtime.lastError);
      }
    });
  });
};

// Create key value object stracture to insert in loacl storage.
var createStorageElement = function( name, value ) {
  var element = {};
  element[name] = value;
  return element;
};

/*
// This is some exapmples on how to use the above code. 
getStorageElementSize( null , function( res ) {
  console.log( 'Local Storage Size = ' + (res/1024).toFixed(2) + 'kbs' );
});

getStorageElement(null, function(res) {
  console.log(res);
});
*/
