/* Trackers Detective - Detect third party domains while you surf the web.
 * Copyright (C) 2015  Costas Iordanou - costasjordanou@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

/*
 *This is the background script. You have one instance for all browser windows|tabs.
 */

// Global and options variables ////////////////////////////////////////
// Options variables //
var OPTION_1 = undefined;
var OPTION_2 = undefined;
var OPTION_OBJ = 'detective_options';
// Global variables //
var debug = false;
var USER_UID = undefined;
var tabsArray = [];
// Badge Colors //
var COLOR_RED = [180, 200, 170, 0];
var COLOR_BLACK = [0, 0, 0, 230];

// Tab dataset object //
var TAB_DATA = function() {
  var TAB = {};
  TAB = ( function() {
    function tab() {};
    tab.prototype.init = function () {
      tab_f = {};
      tab_f.timestamp = null;
      tab_f.pageURL = null;
      tab_f.thirdParties = [];
      tab_f.cookies = [];
      return tab_f;
    };
    return tab;
  })();
  TAB_Dataset = TAB;
}();

// Print debug info to the console//
var trace = function( txt ) {
  if (debug) { console.trace( txt ); }
};
////////////////////////////////////////////////////////////////////////

// Browser state functions /////////////////////////////////////////////
// Executed when browser starts //
chrome.runtime.onStartup.addListener( function() {
  if (debug) console.log( "chrome.runtime.onStartup.addListener() Executed." );
});
///////////////////////////////////////////////////////////////////////

// Cookies and Third parties Related //////////////////////////////////
//Just in case you need this. Note that you need to figure out from
//which tab this event is initiated.
chrome.cookies.onChanged.addListener(function(info){
  /*The underlying reason behind the cookie's change. If a cookie was inserted,
   *or removed via an explicit call to "chrome.cookies.remove", "cause" will be
   *"explicit". If a cookie was automatically removed due to expiry, "cause" will
   *be "expired". If a cookie was removed due to being overwritten with an
   *already-expired expiration date, "cause" will be set to "expired_overwrite".
   *If a cookie was automatically removed due to garbage collection, "cause"
   *will be "evicted". If a cookie was automatically removed due to a "set" call
   *that overwrote it, "cause" will be "overwrite". Plan your response accordingly.
   *source: https://developer.chrome.com/extensions/cookies
   */
  //console.log(info.cause);
  //console.log(info.cookie);
  //console.log(info.removed);
  //if (debug) console.log(info);
});

// Monitor all browser requests (Per browser tab). Note that we
// always miss the initial GET requests initiated by the user,
// but this is not an issue since is always first party request.
chrome.webRequest.onBeforeRequest.addListener( function(request) {
  //Extract the tab id from the request object
  var id = request.tabId
  // Skip chrome internal pages. i.e. chrome://
  if (id < 0) return;
  // Get the corresponding tab.
  chrome.tabs.get( id, function(tab) {
    //Skip the error when the tabs is already closed.
    if (chrome.runtime.lastError) {
      console.log(chrome.runtime.lastError.message);
    }
    try { //Skip internal pages
      if (tab.url.startsWith('chrome')) { return; }
    } catch(e) {console.log(e); return;}
    
    //Extract the tab id from the tabs object
    var tabId = tab.id;
    var dataset = tabsArray[tabId];
    // Check if we created a dataset, if not create it now
    if (dataset == undefined) {
      if (debug) console.log("Dataset is undefined. Let's create it now...");
      createTabRecord(tab);
      dataset = tabsArray[tabId];
    }
    // Extract the domain names for the request and the current
    // tab and compare them to detect third party requests.
    var request_domain = extract_domain(request.url);
    var tab_domain = extract_domain(tab.url);
    if (request_domain != tab_domain) { //Third party requests
  
      /* Important Note:
       * From here you can also collect all the arguments from the url.
       * Usually trackers execute some javascript code and collect data
       * from the browser, then, they forward this information as
       * arguments using GET request.
       * i.e. http://tracker.com?userID=1234&country=US&currency=...
       * If you want to collect them, analize the 'request.url'
       * Check out the bg_support.js file, extract_domain() function
       * on how to manipulate urls in javascript.
       */
  
      //Check if you already record this domain before you add it
      if ( dataset.thirdParties.indexOf( request_domain ) == -1) {
        dataset.thirdParties.push( request_domain );
        if (debug) console.log(tabsArray);
      } 
      if (debug) console.log('+++ Third Party Request +++ URL = ' + request.url + ', tab.url = ' + tab.url);
    }
    //Update the badge text with the number of detected third parties.
    setBadgeText(COLOR_RED, tabId);
  });
}, {urls: ["<all_urls>"]});

//Monitor send headers to detect cookies towards third parties
chrome.webRequest.onBeforeSendHeaders.addListener(function(details) {
  //Extract the tab id from the request object
  var id = details.tabId;
  // Skip chrome internal pages. i.e. chrome://
  if (id < 0) return;
  // Get the corresponding tab.
  chrome.tabs.get( id, function(tab) {
     //Skip the error when the tabs is already closed.
    if (chrome.runtime.lastError) {
      console.log(chrome.runtime.lastError.message);
    }
    try { //Skip internal pages
      if (tab.url.startsWith('chrome')) { return; }
    } catch(e) {console.log(e); return;}
    
    //Extract the tab id from the tabs object
    var tabId = tab.id;
    var dataset = tabsArray[tabId];

    // Extract the domain names for the request and the current
    // tab and compare them to detect third party requests.
    var request_domain = extract_domain(details.url);
    var tab_domain = extract_domain(tab.url);
    if (request_domain != tab_domain) { //Third party requests
      for (var i = 0; i < details.requestHeaders.length; ++i) {
        if (details.requestHeaders[i].name === 'Cookie') {
          cookies = {};
          cookies.domain = request_domain;
          cookies.name = details.requestHeaders[i].name;
          cookies.value = details.requestHeaders[i].value;
          recordCookies(cookies, tabId);
          if (debug) console.log('Cookies leakage detected, from Tab = ' + details.tabId);
        }//end if headers[i].name
      }//end for
    }//end if request_domain != ...
  });
}, {urls: ["<all_urls>"]}, ["requestHeaders"]);

// Tabs Section ///////////////////////////////////////////////////////
// Executed when new tab created //
chrome.tabs.onCreated.addListener( function( tab ) {
  if (debug) console.log( '***** New tab created - tabID = ' + tab.id + ' *****');
  if (debug) console.log(tab);
  //Skip internal chrome pages
  if (tab.url.startsWith('chrome')) {
    delete tabsArray[tab.id];
    return;
  }
  createTabRecord(tab);
  if (debug) console.log(tabsArray);
});

// Executed when tab is updated //
chrome.tabs.onUpdated.addListener( function( tabId, info, tab ) {
  //Check if the tab is pointing to an internal chrome page and delete the dataset.
  if (debug) console.log( '***** Tab updated - tabID = ' + tabId + ' *****');
  if (debug) console.log(tab);
  if (tab.url.startsWith('chrome')) {
    delete tabsArray[tab.id];
    return;
  }
  var oldRecord = tabsArray[tabId];
  //Check if the user navigate to different URL
  if (oldRecord != undefined && oldRecord.pageURL != tab.url) {
    delete tabsArray[tabId];
    createTabRecord(tab);
  } else { createTabRecord(tab); }
  if (debug) console.log(tabsArray);
});

// Create the initial dataset for a tab
var createTabRecord = function(tab) {
  //Check if we already have the dataset...
  if (tabsArray[tab.id] != undefined) return;
  //...if not create a new one...
  var dataset = new TAB_Dataset().init();
  //...populate some information about the tab...
  dataset.pageURL = tab.url;
  var timestamp = Math.round(+new Date()/1000);
  if (debug) console.log(timestamp);
  dataset.timestamp = timestamp;
  //...and save it to the tabsArray.
  tabsArray[tab.id] = dataset;
};

// Executed when a tab is replaced //
chrome.tabs.onReplaced.addListener( function( added, removed ) {
  if (debug) console.log( '***** Tab replaced - removed = ' + removed + ", new = " + added + ' *****');
  //NOTE: If you want to permanently store the tab information,
  //do it here before you delete the dataset. Check also the
  // chrome.tabs.onRemoved listener below.
  //Delete the record for the removed tab.
  delete tabsArray[ removed ];
  //Next create the dataset for the new tab.
  chrome.tabs.get(added, function(tab) {
    createTabRecord(tab);
    if (debug) console.log(tabsArray);
  });
  if (debug) console.log(tabsArray);
});

// Executed when tab is removed //
chrome.tabs.onRemoved.addListener( function( tabId, info ) {
  if (debug) console.log( '***** Tab Removed = ' + tabId + ' *****');
  //NOTE: If you want to permanently store the tab information,
  //do it here before you delete the dataset. Check also the
  // chrome.tabs.onReplaced listener above.
  //Delete the record for the removed tab.
  delete tabsArray[tabId];
  if (debug) console.log(tabsArray);
});

// Executed when the tab is activated //
chrome.tabs.onActivated.addListener( function( info ) {
  if (debug) console.log(info);
  //Just make the bandge color black
  setBadgeText(COLOR_BLACK, info.tabId);
});
////////////////////////////////////////////////////////////////////////

// Messaging between code sections /////////////////////////////////////
//This is how you send the dataset to the popup.js
chrome.runtime.onConnect.addListener(function(port){
  console.assert(port.name == 'dataset');
  port.onMessage.addListener(function(msg){
    //The popup.js request the current active tab dataset
    if (msg.type == 'dataset') {
      //Find the current active tab id
      chrome.tabs.query( { currentWindow: true, active: true }, function(tabs) {
        //Extract the dataset for the current tab
        var dataset = tabsArray[tabs[0].id];
        //And send it to the popup.js
        port.postMessage({response: dataset});
      });
    }
  });
});

// Switch message parsing between code areas (background, tabs)
chrome.runtime.onMessage.addListener(
  function( request, sender, response ) {
    switch ( request.type ) {
      case "options": //From popup.js when options are changed
        console.log( "Options change message from popup.js" );
        checkOptions(); //Update the options variables
        break;
      case "getOptions": //Return the Options values
        console.log( 'inject.js request the options. TabID=' + sender.tab.id );
        var options = {}
        //if you change the variables names here remember
        //to change them also in the popup.js script
        options[OPTION_OBJ] = {"uuid": USER_UID, "option_1": OPTION_1, "option_2": OPTION_2};
        response( options );
        break;
      case "showMessage": //Show message to user if you need to.
        trace( request );
        showUserMessage( request.title, request.message );
        break;
      case "debugMode": //Send the debug mode if asked (popup.js|inject_script.js|etc.)
        response( debug );
        break;
      default: //For debug
        console.log( "Receive unknown internal message." );
        console.log( request );
    }
  }
);
////////////////////////////////////////////////////////////////////////

// Backround Support Functions /////////////////////////////////////////
//Check plugin options from popup.html
var checkOptions = function() {
  var callback = function ( values ) {
    console.log('options values');
    console.log(values[OPTION_OBJ]);
    if ( values[OPTION_OBJ] == undefined ) {
      console.log('Inside options');
      //Initialize localstorage object and call again checkOptions
      var options = {}
      var USER_UID = create_uuid();
      //if you change the variables names here remember
      //to change them also in the popup.js script
      options[OPTION_OBJ] = {"uuid": USER_UID, "option_1": false, "option_2": false};
      console.log(options);
      setStorageElement( options , function() {
        OPTION_1 = false;
        OPTION_2 = false;
        if (debug) console.log('Options object created. USER_UID = ' + USER_UID);
      });
      return;
    }
    //Check if user select any option
    if ( values[OPTION_OBJ][OPTION_1] != undefined && values[OPTION_OBJ][OPTION_1] == "true" ) {
      OPTION_1 = true;
    } else { OPTION_1 = false; }
    if (values[OPTION_OBJ][OPTION_2] != undefined && values[OPTION_OBJ][OPTION_2] == "true" ) {
      OPTION_2 = true;
    } else { OPTION_2 = false; }
    USER_UID = values[OPTION_OBJ].uuid;
  };
  //You call getStorageElement with the above callback.
  //The code executing continues from the above callback.
  getStorageElement(OPTION_OBJ, callback)
};
////////////////////////////////////////////////////////////////////////

// Check for user ID. If not exists, create it. Also load
// the current state of the options. This is executed as
// soon as the background script is fully loaded
checkOptions();

if (debug) console.log('background script loaded.');