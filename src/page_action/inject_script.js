/* Trackers Detective - Detect third party domains while you surf the web.
 * Copyright (C) 2015  Costas Iordanou - costasjordanou@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
 
/* 
 * Use this file to inject javascript code into each individual tab.
 * Currently this file is optional. If you want to delete it remove
 * it also from the manifest file
 */

// Global variables ////////////////////////////////////////////////////
var debug = false;

//Get the debug mode from background
chrome.runtime.sendMessage({ type: "debugMode" }, function (debug_b) {
  debug = debug_b;
  console.log('debug = ' + debug_b);
});

var trace = function( txt ) {
  if (debug) { console.trace( txt ); }
};
////////////////////////////////////////////////////////////////////////

// Events and Listeners ////////////////////////////////////////////////
// Mousescroll event listener sample
document.addEventListener( "mousewheel", function( ev ) {
  if (adsDebug) console.log('Scroll trickered.');
}, false );
////////////////////////////////////////////////////////////////////////

// This code will run when the tab is fully loaded /////////////////////
$( document ).ready( function() {
  if (adsDebug) console.log('document is ready'); 
});
////////////////////////////////////////////////////////////////////////

// Switch message parsing between code areas (background, tabs) ////////
//You can use this if we want to receive messages from other code scripts
chrome.runtime.onMessage.addListener(
  function( request, sender, response ) {
    switch ( request.type ) {
      case "test_type":
        if (debug) console.log( "I receive a test_type message" );
        break;
      default: //For debug
        console.log( "Receive unknown internal message." );
        console.log( request );
    }
  }
);
////////////////////////////////////////////////////////////////////////

// Runtime message handler /////////////////////////////////////////////
var sendMessageToBack = function( type, message, responce ) {
  chrome.runtime.sendMessage( { "type": type, "message": message }, responce );
};
////////////////////////////////////////////////////////////////////////