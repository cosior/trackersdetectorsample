/* Trackers Detective - Detect third party domains while you surf the web.
 * Copyright (C) 2015  Costas Iordanou - costasjordanou@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

//Global variables. Sync this with the backround ones
var OPTION_1 = 'option_1';
var OPTION_2 = 'option_2';
var OPTION_OBJ = 'detective_options';

var debug = false;

//Get the debug mode from background
chrome.runtime.sendMessage({ type: "debugMode" }, function (debug_b) {
  debug = debug_b;
  console.log('debug = ' + debug_b);
});


//Executed when the popup window is fully loaded
window.onload = function () {
  // Get the options from local storage. Note that this
  // is created from the background script if not exists.
  chrome.storage.local.get( OPTION_OBJ, function( data ) {
    if (debug) console.log( data );
    //Check if option 1 checkbox was checked.
    if ( data[OPTION_OBJ][OPTION_1] == "true") {
      //if it was checked mark it as checked...
      document.getElementById( "option_1_id" ).checked = true;
    } else { //...else it was unchecked.
      document.getElementById( "option_1_id" ).checked = false;
    }
    //Same for option 2
    if ( data[OPTION_OBJ][OPTION_2] == "true" ) {
      document.getElementById( "option_2_id" ).checked = true;
    } else {
      document.getElementById( "option_2_id" ).checked = false;
    }
    
    // Add the event listeners to monitor for any checkbox changes from user
    // Event for option 1 checkbox change
    document.querySelector( '#option_1_id' ).addEventListener(
      'change', option1ChangeHandler
    );
    // Event for option 2 checkbox change
    document.querySelector( '#option_2_id' ).addEventListener(
      'change', option2ChangeHandler
    );   
  });
  
  // Check if the current tab has a list of
  // thrid party domains and show it to user
  populateTrackersList();
};

// This code is required if you have links in the popup window.
// Otherwise click is not working
document.addEventListener( "DOMContentLoaded", function () {
  // Allow links to open in new tab
  var links = document.getElementsByTagName( "a" );
  for ( var i = 0; i < links.length; i++ ) {
    ( function () {
      var ln = links[i];
      var location = ln.href;
      ln.onclick = function () {
        chrome.tabs.create( { active: true, url: location } );
      };
    }) ();
  }
});

//Executed when option 1 checkbox is changed
var option1ChangeHandler = function( ev ) {
  chrome.storage.local.get( OPTION_OBJ, function( data ) {
    if ( ev.target.checked == true ) {
      data[OPTION_OBJ][OPTION_1] = true;
      if (debug) console.log( 'Option 1 is selected' );
    } else {
      data[OPTION_OBJ][OPTION_1] = false;
    };
    //Ask background to update the options
    chrome.storage.local.set( data, function() {
      sendMessageToBack( "options", "" );
    });
  });
  if (debug) console.log( ev.target.checked );
};

//Executed when option 2 checkbox changed
var option2ChangeHandler = function( ev ) {
  chrome.storage.local.get( OPTION_OBJ, function( data ) {
    if ( ev.target.checked == true ) {
      data[OPTION_OBJ][OPTION_2] = true;
      if (debug) console.log( 'Option 2 is selected' );
    } else {
      data[OPTION_OBJ][OPTION_2] = false;
    };
    //Ask background to update the options
    chrome.storage.local.set( data, function() {
      sendMessageToBack( "options", "" );
    });
  });
  if (debug) console.log( ev.target.checked );
};

var populateTrackersList = function(){
  //Create a connection with the background script
  var port = chrome.runtime.connect({name: 'dataset'});
  //Request current tab dataset
  port.postMessage({type: 'dataset'});
  //Listen for the response
  port.onMessage.addListener(function(msg){
    if (debug) console.log('port data = ');
    if (debug) console.log(msg.response);
    //If we have a dataset create a table with all third party domains
    if (msg.response != undefined) {
      //Get the list of thirdparty domains from the dataset
      var trackers_list = msg.response.thirdParties;
      //Get the empty div from the popup.html file
      var trackers_div = document.getElementById("trackers");
      //Start building the table with a title on top
      var tbody = '<strong>List of third party domains</strong>\n<div><table border="1">\n';
      //For every thirdparty domain insert a new table row
      for (i = 0; i < trackers_list.length; i++) {
        tbody += '<tr><td>' + trackers_list[i] + '</td></tr>\n';
      }
      //Finally appent the new html code into the trackers div
      trackers_div.innerHTML = tbody;
    }
  });
};

//Helper to send messages to the background
var sendMessageToBack = function( type, message, responce ) {
  chrome.runtime.sendMessage( { "type": type, "message": message }, responce );
};